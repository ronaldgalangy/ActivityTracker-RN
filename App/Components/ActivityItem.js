import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View } from 'react-native'
import styles from './Styles/ActivityItemStyle'
import { Button, Icon, Text, ListItem, Content } from 'native-base'

export default class ActivityItem extends Component {
  // Prop type warnings
  static propTypes = {
    activityName: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    selectedActivity: PropTypes.string.isRequired,
  }
  
  // Defaults for props
  static defaultProps = {
    activityName: '',
    onPress: null,
    selectedActivity: ''
  }

  constructor(props) {
    super(props)
    this.state = {
      isSelected : false,
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log('>>>>>>>>>>>>>>>>>>>>>>>> ', nextProps)
    if(this.props.activityName !== nextProps.activityName) {
      this.setState({ isSelected : false })
    } else {
      this.setState({ isSelected : true })
    }
    
  }

  render () {
    return (
      <ListItem style={{ backgroundColor: 'transparent' }} itemDivider={true}>
        <Content>
          <Button full rounded bordered={this.state.isSelected} danger large onPress={() => this.props.onPress(this.props.activityName)}>
            <Text>{this.props.activityName}</Text>
          </Button>
        </Content>
      </ListItem>
    )
  }
}
