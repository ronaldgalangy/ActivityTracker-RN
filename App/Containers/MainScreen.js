import React, { Component } from 'react'
import { ScrollView, KeyboardAvoidingView, Image } from 'react-native'
import ActivityItem from '../Components/ActivityItem'
import { List, ListItem, Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Drawer, SideBar, Grid, Col, Row, Thumbnail } from 'native-base'

import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/MainScreenStyle'

const menuItems = ['MY PROFILE', 'SCHEDULE', 'EMERGENCY', 'SETTINGS', 'ABOUT THIS APP']
const ACTIVITIES = ['IDLE', 'TRAVEL', 'SALES - POTENTIAL CLIENT', 'SALES - EXISTING CLIENT',
  'SERVICE', 'DOCUMENTATION', 'BREAK']

class MainScreen extends Component {

  constructor(props) {
    super(props)
    this.renderItem = this.renderItem.bind(this)
    this.state = {
      selectedActivity : 'IDLE'
    }
  }

  setSelectedButton = activityName => {
    if (this.state.selectedActivity !== activityName) {
      this.setState({ selectedActivity: activityName })
    }
  }

  renderItem(activityName) {
    return (
      <ActivityItem onPress={this.setSelectedButton.bind(this)} activityName={activityName} selectedActivity={this.state.selectedActivity} galang={this.state.selectedActivity} />)
  }

  render() {
    closeDrawer = () => {
      this.drawer._root.close()
    }

    openDrawer = () => {
      this.drawer._root.open()
    }



    return (
      <Container>
        <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={this._renderContent()}
          panOpenMask={.25}
          onClose={() => closeDrawer()} >
          <Header style={{ backgroundColor: '#F7F8F8' }} androidStatusBarColor={'#c3c3c3'} iosBarStyle={'light-content'}>
            <Left>
              <Button transparent onPress={() => openDrawer()}>
                {/* <Icon ios='ios-menu' android="md-menu" style={{ color: '#202020' }} /> */}
                <Thumbnail resizeMode={'contain'} small square source={{ uri: 'https://www.bpiexpressonline.com/assets/img/mobile-icon-resized.jpg' }}  />
              </Button>
            </Left>
            <Body>
              <Title style={{ color: '#B42B24' }}>Activity Tracker</Title>
            </Body>
            <Right />
          </Header>

          <Content padder style={{ backgroundColor: '#F7F8F8' }}>
            <Grid>
              <Row size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Thumbnail resizeMode={'contain'} square source={{ uri: 'https://www.bpiexpressonline.com/assets/img/mobile-icon-resized.jpg' }}  />
              </Row>
              <Row size={3}>
                <List dataArray={ACTIVITIES}
                  renderRow={this.renderItem}>
                </List>
              </Row>
            </Grid>
          </Content>
        </Drawer>
      </Container>
    )
  }

  _renderContent() {
    return (
      <Grid style={{ backgroundColor: '#f3f3f3' }}>
        <Row size={3} style={{ flexDirection: 'column', backgroundColor: '#EDEDED', justifyContent: 'center', alignItems: 'center' }}>
          <Thumbnail large source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcrQxTGOt1j7Ml6JFvLn3Fg982Ngoxt18U9rEvnz3tL9I-wVXc' }} />
          <Text>Welcome, --name here--</Text>
        </Row>
        <Row size={6} style={{ backgroundColor: 'white' }}>
          <List dataArray={menuItems}
            renderRow={(menuItem) =>
              <ListItem>
                <Text>{menuItem}</Text>
              </ListItem>
            }>
          </List>
        </Row>
        <Row size={1} style={{ backgroundColor: 'white' }}>
          <Content>
            <Button full light>
              <Text>LOG OUT</Text>
            </Button>
          </Content>
        </Row>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen)
